<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { 
    Route::crud('equipments', 'EquipmentCrudController');
    Route::get('equipments/{id}/assign', 'EquipmentCrudController@getAssignForm');
    Route::post('equipment/send', 'EquipmentCrudController@postAssignForm');
    Route::crud('stores', 'StoreCrudController');
    Route::crud('reports', 'ReportCrudController');
    Route::get('reports/{id}/accept','ReportCrudController@accept');
}); 