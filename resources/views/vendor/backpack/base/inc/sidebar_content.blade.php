<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i>Панель управления</a></li>
@if(backpack_user()->hasRole('store-admin'))
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Пользователи</span></a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge"></i> <span>Роли</span></a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Права</span></a></li>
@endif
<li class='nav-item'><a class='nav-link' href="{{ backpack_url('equipments') }}"><i class='nav-icon la la-wrench'></i> Оборудования</a></li>
<li class='nav-item'><a class='nav-link' href="{{ backpack_url('stores') }}"><i class='nav-icon la la-store'></i> Склады</a></li>
<li class='nav-item'><a class='nav-link' href="{{ backpack_url('reports') }}"><i class='nav-icon la la-file-excel-o
'></i>Управление движением / Отчеты</a></li>