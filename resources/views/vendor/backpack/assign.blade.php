@extends(backpack_view('layouts.top_left'))

@section('header')
  <section class="container-fluid">
    <h2>
        
        @if ($crud->hasAccess('list'))
          <small><a href="{{ url($crud->route) }}" class="hidden-print font-sm"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
        @endif
    </h2>
  </section>
@endsection

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
         <form method="post" action="{{ url('/admin/equipment/send') }}">  
          <div class="card">
             <div class="card-header">
                 <h3 class="card-title">Отправка оборудования на другой склад</h3>
             </div>
             <div class="card-body">
              <!-- CSRF token -->
              @csrf
              <input type="number" name="equipment_id" value="{{ $entry->id }}" hidden>
              <input type="number" name="from_store_id" value="{{ $entry->store_id }}" hidden>
              <p>Выберите склад для отправки</p>
                <select name="to_store_id" class="form-control select2-hidden-accessible" required="">
                      @foreach($stores as $s)
                          <option value="{{$s->id}}">{{$s->name}}</option>
                      @endforeach
                </select>
                <br>
                <p>Выберите дату отправки</p>    
                <input name="date" type="text" id="datepicker" required="">
                <br><br>
                <p>Ф.И.О Получателя</p>
                <input type="text" name="reciever" required="">

            </div><!-- /.card-body -->

             <div class="card-footer">
                 <button class="btn btn-success" type="submit">  
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value="save_and_back">Сохранить и выйти</span>
                </button>
             </div><!-- /.card-footer-->
          </div><!-- /.card -->
       </form>
   </div>
</div>
@endsection
