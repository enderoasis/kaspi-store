<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ['equipment_id','equipment_name','equipment_serial_number','equipment_inventory_number','from_store','to_store','send_date','sender_name','reciever_name'];

        Schema::create('reports', function (Blueprint $table) {
            $table->id();
            $table->integer('equipment_id');
            $table->integer('from_store');
            $table->integer('to_store');
            $table->date('send_date');
            $table->string('sender_name');
            $table->string('reciever_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
