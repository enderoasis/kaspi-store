# Kaspi Store

Position: Junior PHP Developer<br>
Deadline: 14 Monday 9:00

# Доступы

По ролям:

Администратор склада:<br>
login: admin@store.kz<br>
pass:  admin2021

Руководитель склада:<br>
login:  manager@store.kz<br>
pass:   manager2021

# Описание

Данное задание выполнено с использованием фреймворка Laravel + Backpack. <br>Стандартные CRUD операции поставлены из под Backpack. Все остальное кастомизировано под требования тестового задания с учетом предметной области.
В Репозитории выложил папку vendor, некоторые файлы там изменены (блэйды,скрипты).
	
	Путеводитель по джунглям:
	app/Http/Controller/Admin - логику искать тут, отрисовка визуального интерфейса, операции, CRUD
	routes/backpack/custom - роуты.
	resources/views/vendor/backpack/base/inc/sidebar_content.blade.php

# Demo

Для удобства задеплоил проект по адресу:  [Нажмите сюда](http://195.210.46.10/plesk-site-preview/store.lombarbot.kz/https/195.210.46.10)

