<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\EquipmentRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class EquipmentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EquipmentCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */

    public function setup()
    {
        CRUD::setModel(\App\Models\Equipment::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/equipments');
        CRUD::setEntityNameStrings('оборудование', 'Оборудования');
        $this->crud->enableBulkActions();

        if (backpack_user()->hasRole('store-admin'))
            {
                CRUD::denyAccess(['create', 'update','delete']);
                $this->crud->enableExportButtons();   
            }
        if (backpack_user()->hasRole('store-manager'))
            {
                $this->crud->addButtonFromView('line', 'assign', 'assign', 'beginning');   
            }
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        CRUD::addColumns([
            [
                'name' => 'id',
                'label' => 'id',
                'type' => 'text',
            ],
            [
                'name' => 'name',
                'label' => trans('Наименование оборудования'),
                'type' => 'text'
            ],
            [
                'name' => 'serial_number',
                'label' => trans('Серийный номер'),
                'type' => 'text'
            ],
            [
                'name' => 'inventory_number',
                'label' => trans('Инвентарный номер'),
                'type' => 'text'
            ],
            [
                'name' => 'store',
                'label' => trans('Склад'),
                'type' => 'relationship'
            ],
            [
                'name' => 'created_at',
                'label' => trans('Создано'),
                'type' => 'datetime'
            ],
            [
                'name' => 'updated_at',
                'label' => trans('Обновлено'),
                'type' => 'datetime',
            ]
        ]);

    }

    public function getAssignForm($id) 
    
    {
            
            $this->crud->hasAccessOrFail('update');
            $this->crud->setOperation('Assign');

            $this->data['entry'] = $this->crud->getEntry($id);
            $this->data['crud'] = $this->crud;
            $this->data['title'] = 'Отправка оборудования '.$this->crud->entity_name;
            $this->data['current_store'] = \App\Models\Store::find($this->data['entry']->store_id);
            $this->data['stores'] = \App\Models\Store::all();
             
            return view('vendor.backpack.assign', $this->data);
    }

    public function postAssignForm()
    {
           
            // Переназначение склада
            $equipment = \App\Models\Equipment::findorfail(request('equipment_id'))
            ->update([
            'store_id' => request('to_store_id')
            ]);
            // Для отчетности периода движения оборудования
            $report = \App\Models\Report::create([
            'equipment_id' => request('equipment_id'),
            'from_store' => request('from_store_id'),
            'to_store' => request('to_store_id'),
            'sender_name' => backpack_user()->name,
            'reciever_name' => request('reciever'),
            'send_date' => request('date'),
            'status' => 'Отправлено'
            ]);

            // Сообщение об успехе
            \Alert::success('Оборудование отправлено на указанный склад')->flash();

            return \Redirect::to($this->crud->route);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::addFields([
            [
                'name' => 'name',
                'label' => trans('Наименование оборудования'),
                'type' => 'text',
                'attributes' => [
                    'required' => true,
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-sm-12 required'
                ],
            ],
            
            [
                'name' => 'serial_number',
                'label' => trans('Серийный номер'),
                'attributes' => [
                    'required' => true,
                ],
                'type' => 'number',
                'tab' => trans('Основные данные'),
            ],
            [
                'name' => 'inventory_number',
                'label' => trans('Инвентарный номер'),
                'attributes' => [
                    'required' => true,
                ],
                'type' => 'number',
                'tab' => trans('Основные данные'),
            ],
            [
                'name' => 'store_id',
                'label' => trans('Склад'),
                'model' => "App\Models\Store",
                'placeholder' => "Выберите склад",
                'attributes' => [
                    'required' => true,
                ],
                'type' => 'relationship',
                'tab' => trans('Склад')
            ]
        ]);

    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::addFields([
            [
                'name' => 'name',
                'label' => trans('Наименование оборудования'),
                'type' => 'text',
                'attributes' => [
                    'required' => true,
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-sm-12 required'
                ],
            ],
            
            [
                'name' => 'serial_number',
                'label' => trans('Серийный номер'),
                'attributes' => [
                    'required' => true,
                ],
                'type' => 'number',
                'tab' => trans('Основные данные'),
            ],
            [
                'name' => 'inventory_number',
                'label' => trans('Инвентарный номер'),
                'attributes' => [
                    'required' => true,
                ],
                'type' => 'number',
                'tab' => trans('Основные данные'),
            ]
        ]);


    }
}
