<?php

namespace App\Http\Controllers\Admin;

use App\Models\Report;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;

/**
 * Class ReportCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ReportCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;


    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */

    public function setup()
    {
         CRUD::setModel(\App\Models\Report::class);
         CRUD::setRoute(config('backpack.base.route_prefix') . '/reports');
         CRUD::setEntityNameStrings('отчёт', 'Отчёты');
         CRUD::denyAccess(['create', 'update','delete','show']);
        $this->crud->orderby('status');

        if (backpack_user()->hasRole('store-admin'))
            {
                // Excel export
                 $this->crud->enableExportButtons(['visibleInTable' => true]);

                // Поисковик по промежутку времени. Экспорт записей по итогам.
                 $this->crud->addFilter([
                    'label' => 'Выберите промежуток времени движения',
                    'type'  => 'date_range',
                    'name'  => 'send_date',
                ],false, function ($value) {
                    $dates = json_decode($value);
                    $this->crud->addClause('where', 'send_date', '>=', $dates->from);
                    $this->crud->addClause('where', 'send_date', '<=', $dates->to . " 23:59:59");
                });   
            }

        if (backpack_user()->hasRole('store-manager'))
            {
                // Кнопка принятия оборудования в разделе Управления движением
                $this->crud->addButtonFromView('line', 'accept', 'accept', 'beginning');
            }

      }

    public function accept()
    {
           
           $report = Report::findorfail(request('id'))
           ->update([
            'status' => 'Принято',
            'accept_date' => Carbon::now()
           ]);
 
            \Alert::success('Оборудование принято!')->flash();

            return \Redirect::to($this->crud->route);
    }



    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {  

        CRUD::addColumns([
            [
                'name' => 'id',
                'label' => 'id',
                'type' => 'text',
                'data-visible' => false,
            ],
            [
                'name' => 'equipment.name',
                'label' => trans('Наименование оборудования'),
                'type' => 'text',

            ],
            [
                'name' => 'equipment.serial_number',
                'label' => trans('Серийный номер'),
                'type' => 'text'
            ],
            [
                'name' => 'equipment.inventory_number',
                'label' => trans('Инвентарный номер'),
                'type' => 'text'
            ],
            [
                'name' => 'from_store',
                'label' => trans('Из склада'),
                'type' => 'text'
            ],
            [
                'name' => 'to_store',
                'label' => trans('В склад'),
                'type' => 'text'
            ],
            [
                'name' => 'send_date',
                'label' => trans('Отправлено'),
                'type' => 'text'
            ],
            [
                'name' => 'sender_name',
                'label' => trans('Отправитель'),
                'type' => 'text',
            ],
            [
                'name' => 'reciever_name',
                'label' => trans('Получатель'),
                'type' => 'text',
            ],
            [
                'name' => 'status',
                'label' => trans('Статус'),
                'type' => 'text'
            ],
            [
                'name' => 'accept_date',
                'label' => trans('Дата приема'),
                'type' => 'text'
            ]
        ]);

    }

    protected function setupCreateOperation()
    {
        //
    }

    protected function setupUpdateOperation()
    {
        //
    }
}
