<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;


class Report extends Model
{
    use CrudTrait;


    protected $fillable = ['equipment_id','equipment_name','equipment_serial_number','equipment_inventory_number','from_store','to_store','send_date','sender_name','reciever_name','accept_date','status'];

    public function equipment()
    {
    	return $this->belongsTo('App\Models\Equipment');
    }

    protected $casts = [
        'send_date' => 'date',
    ];
 
}
